import React, { Component } from 'react';
import { 
  View, ActivityIndicator, TextInput, TouchableOpacity, Text, Linking,
  Button, FlatList, ScrollView
} from 'react-native';
import axios from 'axios';
import Video from 'react-native-vector-icons/FontAwesome5'; 
import { OTSession, OTPublisher, OTSubscriber, OTSubscriberView } from 'opentok-react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apiKey: '46480622',
      sessionId: '1_MX40NjQ4MDYyMn5-MTU3Nzk1NDA3NjAzMX5ZZkxOc00zUXhhRW9LbGFaSUJZTmhQaTB-UH4',
      token: '',
      isConnected: false,
      loading: false,
      isShowChat: false,
      sessionConnected: false,
      sessionDisconnected: false,
      streamProperties: {},
      publisherProperties: {},
      signal: {
        data: '',
        type: 'chat',
      },
      text: 'Hello Chat Usama',
      messages: [],
    }
    this.sessionOptions = {
      connectionEventsSuppressed: true, // default is false
      androidZOrder: 'mediaOverlay', // Android only - valid options are 'mediaOverlay' or 'onTop'
      androidOnTop: 'publisher',  // Android only - valid options are 'publisher' or 'subscriber'
      useTextureViews: true,  // Android only - default is false
      isCamera2Capable: true, // Android only - default is false
    };

    this.subscriberProperties = {
      subscribeToAudio: false,
      subscribeToVideo: false,
    };   

    this.publisherProperties = {
      name: 'usama butt',
      publishVideo: true,
      publishAudio: false,
      cameraPosition: 'front'
    };

    // works
    this.otSessionRef = React.createRef();
    this.sessionEventHandlers = {
      signal: (event) => {
        console.log('signal---->>>>',event);
        if (event.data) {
          const myConnectionId = this.session.getSessionInfo().connection.connectionId;
          console.log('connectionID===>>>',myConnectionId);
          const oldMessages = this.state.messages;
          console.log('previous Chat===>>>',this.state.messages);
          console.log('eventData===>>>',event.data);
          const messages = event.connectionId === myConnectionId ? [...oldMessages, {data: `Me: ${event.data}`}] : [...oldMessages, {data: `Other: ${event.data}`}];
          this.setState({
            messages,
          });
        }
      },
      streamCreated: event => {
        const streamProperties = {
          ...this.state.streamProperties, [event.streamId]: {
            subscribeToAudio: true,
            subscribeToVideo: true,
            style: {
              width: '100%',
              height: '100%',
            },
          }
        };
        this.setState({ streamProperties });
      },
      streamDestroyed: event => {
        console.log('Stream destroyed!', event);
      },
      sessionConnected: event => {
        this.setState({
          isConnected: true,
          loading: false,
          sessionDisconnected: false
        })
      },
      sessionDisconnected: event => {
        this.setState({
          // isConnected: false,
          sessionDisconnected: true
        })
      },
    };
    this.publisherEventHandlers = {
      streamCreated: event => {
        console.log('Publisher stream created!', event);
        const publisherProperties = {
          name: 'usama butt',
          publishVideo: true,
          publishAudio: false,
          cameraPosition: 'front'
        }
        this.setState({ publisherProperties })
      },
      streamDestroyed: event => {
        console.log('Publisher stream destroyed!', event);
      }
    };
    this.subscriberEventHandlers = {
      error: (error) => {
        console.warn(`There was an error with the subscriber: ${error}`);
      },
      connected: (obj) => {
        console.warn('connecter:', obj);
        this.setState({ isConnected: true })
      },
      disconnected: (obj) => {
        this.setState({ isConnected: false })
      },
      videoEnabled: () => {
        alert('Video enabled');
      },
      videoDisabled: () => {
        alert('Video disabled');
      }
    };
  }
  toggleVideo = () => {
    const publisherProperties = {
      name: 'usama butt',
      publishVideo: !this.state.publisherProperties.publishVideo, //this.state.publisherProperties.publishVideo
      publishAudio: !this.state.publisherProperties.publishAudio,
      cameraPosition: 'front'
    }
    this.setState({ publisherProperties })
  }
  sendSignal() {
    if (this.state.text) {
      this.setState({
        signal: {
          type: 'chat',
          data: this.state.text,
        },
        text: '',
      });
    }
  }
  _keyExtractor = (item, index) => index;
  _renderItem = ({item}) => (
    <Text style={{ padding: 10,fontSize: 18,height: 44, color: '#000'}}>{item.data}</Text>
  );

  joinSession = async () => {
    this.setState({ loading: true })
    var SERVER_BASE_URL = 'https://87b45bcf.ngrok.io';
    // var SERVER_BASE_URL = 'https://nuraniah-opentok-rtc.herokuapp.com';
    axios.get(SERVER_BASE_URL + '/otok/join-session/' + this.state.sessionId)
      .then((res) => {
        const { sessionId, generatedToken } = res.data;
        console.warn('res----->>>>>', res);
        this.setState({
          sessionId,
          token: generatedToken,
          // loading: false,
          sessionConnected: true
        })
      })
      .catch(function (error) {
        console.log('errrrro===>>>>', error);
        this.setState({ loading: false })
      });
  } 

  link=(url)=>{
    Linking.canOpenURL(url)
  .then((supported) => {
    if (!supported) {
      console.log("Can't handle url: " + url);
    } else {
      return Linking.openURL(url);
    }
  })
  .catch((err) => console.error('An error occurred', err));
  }
  render() {
    const { sessionConnected, loading, apiKey, sessionId, token, isConnected, sessionDisconnected, publisherProperties } = this.state;
    return (
      <View style={{ flex: 1 }}>
        {
          sessionConnected ?
            <OTSession 
              apiKey={apiKey} 
              sessionId={sessionId} 
              token={token} 
              signal={this.state.signal}
              options={this.sessionOptions} 
              eventHandlers={this.sessionEventHandlers} 
              // ref={this.otSessionRef}
              ref={(instance) => {
                this.session = instance;
              }}
              >
              {
                isConnected ?
                  <OTSubscriber
                    properties={this.subscriberProperties}
                    eventHandlers={this.subscriberEventHandlers}
                    style={{ height: '100%', width: '100%' }}
                    streamProperties={this.state.streamProperties}
                  />
                  :
                  <View style={{ flex: 1,justifyContent:'center',alignItems:'center' }}>
                    {
                      sessionDisconnected ?
                        <Text style={{ color: 'black', fontSize: 14 }}>session is destroy</Text>
                        :
                        <Text style={{ color: 'black', fontSize: 14 }}>session is creating..</Text>
                    }
                  </View>
              } 
              {/* <OTSubscriber
                properties={this.subscriberProperties}
                eventHandlers={this.subscriberEventHandlers}
                style={{ height: '100%', width: '100%' }}
                streamProperties={this.state.streamProperties}
              /> */}
              <View style={{ height: '100%', width: '100%' , position:'absolute'}}>
                <View style={{ height: '20%', width: '100%', alignItems:'flex-start', justifyContent:'flex-end', }}>
                  <OTPublisher
                    style={{ width: 100, height: 100, position: 'absolute', zIndex: 10, marginVertical: 10, marginHorizontal: 10 }}
                    properties={this.state.publisherProperties}
                    eventHandlers={this.publisherEventHandlers}
                  />
                  <TouchableOpacity 
                    style={{ height: 30, width: 30,backgroundColor:'#fff',borderRadius: 50 ,position:'absolute', marginTop: 75, marginLeft: 45, justifyContent:'center', alignItems:'center' }} 
                    onPress={()=> this.toggleVideo()}
                    >
                      <Video name={ publisherProperties.publishVideo? 'video':'video-slash'} size={14} color='#000' />
                  </TouchableOpacity>
                </View>
                <View style={{ height: '80%', width: '100%', backgroundColor:'rgba(255,255,255,0.5)'}}>
                  <FlatList
                    data={this.state.messages}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                  />
                  <View style={{ height: '10%', width: '100%', flexDirection: 'row',justifyContent:'center', alignItems:'center', backgroundColor:'#fff' }}>
                    <TextInput
                      style={{height: 40, width: '70%', borderColor: 'gray', marginRight: 10,borderWidth: 1, backgroundColor:'#fff', borderRadius: 5}}
                      onChangeText={(text) => { this.setState({ text }); }}
                      value={this.state.text}
                    />
                    <Button
                      onPress={() => { this.sendSignal(); }}
                      // style={{height: 40, width: '10%', borderRadius: 5, marginLeft: 10 }}
                      title="Send"
                    />
                  </View>
                </View>
              </View>
              
            </OTSession>
            :
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              {
                loading ?
                  <ActivityIndicator size='large' color='black' animating={true} />
                  :
                  <View >
                    <TextInput
                      placeholder='enter sessionID'
                      onChangeText={(value) => this.setState({ sessionId: value })}
                      onSubmitEditing={() => this.joinSession()}
                      style={{ height: 50, width: 250, borderRadius: 2, borderColor: 'black', borderWidth: 0.6, paddingHorizontal: 10 }}
                    />
                    <TouchableOpacity
                      style={{ height: 50, width: 250, backgroundColor: '#000', marginVertical: 15, alignItems: 'center', justifyContent: 'center' }}
                      onPress={() => this.joinSession()}
                    >
                      <Text style={{ fontSize: 14, color: '#fff' }}>Join</Text>
                    </TouchableOpacity>
                  </View>
              }
            </View>
        }
      </View>
    );
  }
}